'use strict';

app.controller('MyCtrl', function($scope, Data, _) {
    $scope.rowCollection = {};
    $scope.displayedCollection = {};

    Data.getList().then(function(data) {
        $scope.data = data;
        $scope.displayedCollection = _.groupBy(data, function(user){
            return user.name;
        });
        for (var key in $scope.displayedCollection) {
            $scope.hiddenGroups.push(key);
        }
    });

    $scope.sortBy = function(field) {
        if ($scope.sortField === field) {
            _.each($scope.displayedCollection, function(group, key) {
                $scope.displayedCollection[key].reverse();
            });
        }
        else {
            $scope.sortField = field;
            _.each($scope.displayedCollection, function(group, key) {
                $scope.displayedCollection[key] = _.sortBy(group, field);
            });
        }
    };

    $scope.toggleColumn = function(name) {
        var hiddenColumns = $scope.hiddenColumns,
            enabledColumns = $scope.enabledColumns;

        hiddenColumns.indexOf(name) !== -1 
            ? enableColumn(hiddenColumns, enabledColumns, name)
            : hideColumn(hiddenColumns, enabledColumns, name);
    };

    //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
    $scope.displayedCollection = angular.copy($scope.rowCollection, {});

    $scope.sortField = null;
    $scope.hiddenGroups = [];
    $scope.hiddenColumns = [];
    $scope.enabledColumns = [];
    $scope.isCollapsed = true;

    function hideColumn(hiddenColumns, enabledColumns, name) {
        if (enabledColumns.indexOf(name) !== -1) {
            enabledColumns.splice(enabledColumns.indexOf(name), 1);
        }
        hiddenColumns.push(name);
    }

    function enableColumn(hiddenColumns, enabledColumns, name) {
        hiddenColumns.splice(hiddenColumns.indexOf(name), 1);
        enabledColumns.length = 0;
        enabledColumns.push(name);
    }
});


