'use strict';

app.factory('Data', function($http) {
    var me = this;

    me.getList = function() {
        return $http.get('profiles.json').then(function(incomingData) {

            return incomingData.data;
        });
    };

    return me;
});
