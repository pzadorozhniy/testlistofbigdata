'use strict';

app.directive('toggleGroup', function() {
    return {
        restrict: 'A',

        link: function(scope, element, attrs, controller) {
            scope.toggleGroup = function(key) {
                if (scope.hiddenGroups.indexOf(key) === -1) {
                    scope.hiddenGroups.push(key);
                    $('[data-group-name="' + key +'"] tr' ).addClass('disabled');
                }
                else {
                    scope.hiddenGroups.splice(scope.hiddenGroups.indexOf(key), 1);
                    $('[data-group-name="' + key +'"] tr' ).removeClass('disabled');
                }
            };
        }
    }
});