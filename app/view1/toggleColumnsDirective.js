'use strict';

app.directive('toggleColumns', function($compile) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs, controller) {
            attrs.$observe('hiddenColumns', function() {
                for (var i = scope.hiddenColumns.length - 1; i >= 0; i--) {
                    $('[data-column-name=' + scope.hiddenColumns[i]+']').hide();
                }
            });
            attrs.$observe('enabledColumns', function() {
                for (var i = scope.enabledColumns.length - 1; i >= 0; i--) {
                    $('[data-column-name=' + scope.enabledColumns[i]+']').show();
                }
            });
        }
    }
});