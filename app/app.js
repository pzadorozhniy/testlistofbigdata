'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myApp', [
    'ngRoute',
    'smart-table',
    'ui.bootstrap',
    'underscore'
])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/view1', {
        templateUrl: 'view1/view.html',
        controller: 'MyCtrl'
    });
    $routeProvider.otherwise({redirectTo: '/view1'});
}])
