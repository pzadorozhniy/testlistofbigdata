### Install Dependencies

```
npm install
```
```
bower install
```

### Run the Application

```
npm start
```

Now browse to the app at `http://localhost:8000/app`.
